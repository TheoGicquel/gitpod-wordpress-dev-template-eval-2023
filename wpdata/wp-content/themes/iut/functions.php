<?php
/**
* functions.php
* @author TheoGiquel
*/

add_action( 'wp_enqueue_scripts', 'iut_enqueue_styles');

function iut_enqueue_styles() {
	$parenthandle = 'parent-style'; // This is 'twentynineteen-style' that we use as a base for our IUT style
	$theme = wp_get_theme();
	wp_enqueue_style( $parenthandle,
		get_template_directory_uri() . '/style.css',
		array(), // If the parent theme code has a dependency, copy it to here.
		$theme->parent()->get( 'Version' )
);

	wp_enqueue_style( 'child-style',
		get_stylesheet_uri(),
		array( $parenthandle ),
		$theme->get( 'Version' )
);
}

// register custom type

function recipe_custom_post_type(){
register_post_type(
    'recipe',
    // flavor text for menu
    array(
        'labels'				=> array(
            'name' => _x('Posts : Recettes','Post Type General Name'),
            'singular_name' => _x('Poste Recette','Post Type Singular Name'),
            'menu_name' => __('Recettes'),
            'all_items' => __('Toutes les recettes'),
            'view_item' => __('Consulter recette'),
        ),
        'description'			=> 'Recettes',
        'public'				=> true,    // false = cachée de l'interface d'admin et du frontend
        'publicly_queryable'	=> true,    // Visible côté frontend ?
        'exclude_from_search'	=> false,   // Exclure de la recherche ?
        'show_ui'				=> true,    // Montrer l'interface dans l'admin
        'show_in_rest'			=> true,	// Nécessaire pour fonctionner avec Gutenberg
        'has_archive'           => "recettes",
        'rewrite'               => array( 'slug' => 'recette' ),
    )
);
}

add_action('init', 'recipe_custom_post_type');

// metabox
function recipe_info_add_custom_box() {
		add_meta_box(
			'recipe_box_id',                 // Unique ID
			'Informations Complémentaires',      // Box title
			'recipe_info_custom_box_html',  // Content callback, must be of type callable
            'recipe', // type
            'normal',
            'default'
		);
}

add_action( 'add_meta_boxes', 'recipe_info_add_custom_box' );

function recipe_info_custom_box_html($post) {
    
    // reload saved ingredients
    $ingredients = get_post_meta($post->ID, 'ingredients', true);
    
    // php hurts my eyes
    // -- TEMPLATE 
    ?>
    <p> Ingredients</p>
    <textarea name="ingredients" rows="5" cols="40">
        <?php echo $ingredients; ?>
    </textarea>
    
    <?php
    // -- /TEMPLATE 

}


function save_recipe_info_meta_box($post_id) {
	if ( array_key_exists( 'ingredients', $_POST ) ) {
    update_post_meta(
            $post_id,
            'ingredients',
			$_POST['ingredients']
        );
    }
}
add_action( 'save_post', 'save_recipe_info_meta_box' );


?>